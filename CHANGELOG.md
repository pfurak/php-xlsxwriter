# Changelog
All notable changes to this project will be documented in this file.

## [v1.3.6] - 2023-02-02
### Added
- Added `examples/index.php` as a new example

### Changed
- Modify zip open logic to avoid `Deprecated` warning in `XLSXBuilder` class
- Fixed a typo that said this package supports PHP 7.0+ in `README.md` and `composer.json`

## [v1.3.5] - 2023-01-19
### Added
#### To support hyperlink on same sheet that contains image and/or chart
- Added `examples/example_chart_image_link.php` as a new example

### Changed
- Modify HyperLink ID logic in `initHyperLink` funciton in class `Sheet`

## [v1.3.4] - 2022-12-21
### Added
#### To support row height and column width
- Added `examples/example_row_height_column_width.php` as a new example
- Added `setRowHeight` and `setColumnWidth` function into `Export` and `Sheet` classes
- Added `getColWidths` function into `Sheet` class

### Added
#### To support hyperlinks
- Added `examples/example_hyperlinks.php` as a new example
- Added `Hyperlink` class
- Added `initHyperLink` function into `Sheet` class

### Changed
- Modify param types of `addField` and `writeCell` functions in classes `Export` and `Sheet`
- Modify `buildWorkSheets` logic in class `XLSXBuilder`

## [v1.3.3] - 2022-09-11
### Added
- Added `examples/example_filters.php` as a new example
- Added `setFilter` function into `Export` and `Sheet` classes
- Added `getFilter` function into `Sheet` class

### Changed
- Modify `Sheet` class to use `getFilter` function in `getXMLFooter` function

## [v1.3.2] - 2022-09-10
### Added
- Added `examples/example_fixed_row_column.php` as a new example
- Added `markFixedTopRows` and `markFixedLeftColumns` functions into `Export` and `Sheet` classes
- Added `getSheetViews` function into `Sheet` class

### Changed
- Modify `Sheet` class to use `getSheetViews` function in `getXMLHeader` function

## [v1.3.1] - 2022-08-17
### Added
- Added `FileWriter` and `XLSXBuilder` classes
- Added new constructor parameter into `Export` class (`useTempFiles` as a boolean variable)
- Added `examples/example_huge_data.php` as a new example

### Changed
- Modify `Export` class to use `XLSXBuilder` class when generating output
- Modify `setEncoding` function in `Export` class: Cannot modify the encoding if `useTempFiles` is `true`
- Modify `SharedStrings` and `Sheet` classes to use `FileWriter` to store data if `useTempFiles` is `true`

## [v1.3] - 2022-05-19
### Added
- Support Pie Chart
  - Added `PieChart` class

## [v1.2] - 2022-05-12
### Changed
- Modify `LineChart` and `Line` classes to support X axis
- Add parameter type into `getXML` function in `LineChart` and `BarChart` classes

## [v1.1] - 2022-05-11
### Added
- Support Bar Chart
  - Added `BarChart` and `Bar` classes

### Changed
- Fixed `double` and `float` type in `Sheet` class

## [v1.0] - 2022-05-09
### Added