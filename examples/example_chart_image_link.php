<?php

require_once __DIR__ . '/../vendor/autoload.php'; // Autoload files using Composer autoload

use XLSXWriter\Export;
use XLSXWriter\Chart\LineChart\LineChart;
use XLSXWriter\Chart\LineChart\Line;
use XLSXWriter\Hyperlink;
use XLSXWriter\Font;

$output = strtolower(basename(__FILE__, ".php")) . ".xlsx";
if(file_exists($output)) {
    unlink($output);
}

$imgDir = __DIR__ . '/../images/';
$images = [[
    "file"  =>  $imgDir . 'img.png',
    "row"   =>  19,
    "col"   =>  0
], [
    "file"  =>  $imgDir . 'img_2.png',
    "row"   =>  19,
    "col"   =>  2
], [
    "file"  =>  $imgDir . 'img_3.png',
    "row"   =>  19,
    "col"   =>  4
]];

$sheets = [ "sheet1", "sheet2" ];

$hyperLinks = [
    new Hyperlink("https://google.com", "Google Tooltip", "Google"),
    new Hyperlink("../images/custom_number_format.png", "Custom Number Format Image Tooltip", "Custom Number Format Image"),
    // new Hyperlink(["sheetName" => "sheet2", "row" => 0, "col" => 0], "Another Sheet Column Tooltip", "Another Sheet Column")
];

$linkStyle = [
    'font' => new Font([ "underline" => true, "color" => ["from" => "string", "value" => "blue"] ])
];

$style = ["numberFormat" => "#,##0.00\ [\$Ft-hu-HU]"];

$EURToHUF = ["EUR TO HUF", 380.71, 381.92, 382.10, 382.56, 378.65];
$USDToHUF = ["USD TO HUF", 362.15, 362.15, 362.15, 365.00, 360.87]; 
$dates = ["Date", "2022.05.06.", "2022.05.07.", "2022.05.08.", "2022.05.09.", "2022.05.10."];

$data = [$EURToHUF, $USDToHUF, $dates];
$maxRow = count($data);

// Normal mode
$export = new Export($output);

foreach($sheets as $sheetName) {
    $chart = new LineChart();
    $chart->setTitle("EUR (€) & USD ($) to HUF (Ft)");
    $chart->setXAxis($sheetName, $maxRow - 1, 1, count($data[$maxRow - 1]) - 1);
    $chart->setPosition($maxRow, 0);
    $chart->setShowLegend(true);
    $chart->setLegendPosition('b'); // bottom

    for($row = 0; $row < $maxRow; $row++) {
        $dataRow = $data[$row];
        $maxData = count($dataRow);
        for($col = 0; $col < $maxData; $col++) {
            $value = $dataRow[$col];
            $s = $col > 0 && $row < $maxRow - 1 ? $style : [];
            $export->addField($sheetName, $row, $col, $value, $s);
        }
    
        if($row < $maxRow - 1) {
            $line = new Line();
            $line->setTitle($sheetName, $row, 0);
            $line->setData($sheetName, $row, 1, $maxData - 1);
            $chart->addLine($line);
        }
    }

    $export->addChart($sheetName, $chart);

    foreach($images as $image) {
        $export->addImage($sheetName, $image['file'], $image['row'], $image['col']);
    }

    $row = 19;
    $col = 6;
    foreach($hyperLinks as $hyperLink) {
        $export->addField($sheetName, $row, $col++, $hyperLink, $linkStyle);
    }

    $tmpSheetName = $sheetName === "sheet1" ? "sheet2" : "sheet1";
    $hyperLink = new Hyperlink(["sheetName" => $tmpSheetName, "row" => 0, "col" => 0], "Another Sheet Column Tooltip", "Another Sheet Column");
    $export->addField($sheetName, $row, $col++, $hyperLink, $linkStyle);
}

$export->saveOnDisk(__DIR__);
unset($export);

// With Temp Files

$output = strtolower(basename(__FILE__, ".php")) . "_temp_files.xlsx";
if(file_exists($output)) {
    unlink($output);
}

$useTempFiles = true; // add this parameter if you want to work with huge data
$export = new Export($output, Export::DEFAULT_ENCODING, $useTempFiles);

foreach($sheets as $sheetName) {
    $chart = new LineChart();
    $chart->setTitle("EUR (€) & USD ($) to HUF (Ft)");
    $chart->setXAxis($sheetName, $maxRow - 1, 1, count($data[$maxRow - 1]) - 1);
    $chart->setPosition($maxRow, 0);
    $chart->setShowLegend(true);
    $chart->setLegendPosition('b'); // bottom

    for($row = 0; $row < $maxRow; $row++) {
        $export->openRow($sheetName, $row);

        $dataRow = $data[$row];
        $maxData = count($dataRow);
        for($col = 0; $col < $maxData; $col++) {
            $value = $dataRow[$col];
            $s = $col > 0 && $row < $maxRow - 1 ? $style : [];

            $export->writeCell($sheetName, $col, $value, $style);
        }
    
        if($row < $maxRow - 1) {
            $line = new Line();
            $line->setTitle($sheetName, $row, 0);
            $line->setData($sheetName, $row, 1, $maxData - 1);
            $chart->addLine($line);
        }

        $export->closeCurrentRow($sheetName);
    }

    $export->addChart($sheetName, $chart);

    foreach($images as $image) {
        $export->addImage($sheetName, $image['file'], $image['row'], $image['col']);
    }

    $row = 19;
    $export->openRow($sheetName, $row);
    $col = 6;
    foreach($hyperLinks as $hyperLink) {
        $export->writeCell($sheetName, $col++, $hyperLink, $linkStyle);
    }

    $tmpSheetName = $sheetName === "sheet1" ? "sheet2" : "sheet1";
    $hyperLink = new Hyperlink(["sheetName" => $tmpSheetName, "row" => 0, "col" => 0], "Another Sheet Column Tooltip", "Another Sheet Column");
    $export->writeCell($sheetName, $col++, $hyperLink, $linkStyle);

    $export->closeCurrentRow($sheetName);
}

$export->saveOnDisk(__DIR__);
unset($export);