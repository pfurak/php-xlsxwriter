<?php

require_once __DIR__ . '/../vendor/autoload.php'; // Autoload files using Composer autoload

use XLSXWriter\Export;

$output = strtolower(basename(__FILE__, ".php")) . ".xlsx";
if(file_exists($output)) {
    unlink($output);
}

$secondOutput = strtolower(basename(__FILE__, ".php")) . "_2.xlsx";
if(file_exists($secondOutput)) {
    unlink($secondOutput);
}

$sheetNames = [ "sheet_1", "sheet_2" ];
$maxRow = 300;
$maxCol = 100;

$export = new Export($output);

foreach($sheetNames as $sheetName) {
    $export->markFixedTopRows($sheetName, 4);
    $export->markFixedLeftColumns($sheetName, 4);

    for($row = 0; $row < $maxRow; $row++) {
        for($col = 0; $col < $maxCol; $col++) {
            $value = getValue($col);
            $export->addField($sheetName, $row, $col, $value);
        }
    }
}

$export->saveOnDisk(__DIR__);
unset($export);

$useTempFiles = true; // add this parameter if you want to work with huge data
$export = new Export($secondOutput, Export::DEFAULT_ENCODING, $useTempFiles);

foreach($sheetNames as $sheetName) {
    // need to call these functions before opening the first row of the current sheet
    $export->markFixedTopRows($sheetName, 4);
    $export->markFixedLeftColumns($sheetName, 4);

    for($row = 0; $row < $maxRow; $row++) {
        $export->openRow($sheetName, $row);

        for($col = 0; $col < $maxCol; $col++) {
            $value = getValue($col);
            $export->writeCell($sheetName, $col, $value);
        }

        $export->closeCurrentRow($sheetName);
    }
}

$export->saveOnDisk(__DIR__);
unset($export);

function getValue(int $col) : int | string {
    if($col < 5) {
        return "CELL_" . ($col + 1);
    }

    return (int)($col + 1);
}