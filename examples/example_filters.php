<?php

require_once __DIR__ . '/../vendor/autoload.php'; // Autoload files using Composer autoload

use XLSXWriter\Export;

$output = strtolower(basename(__FILE__, ".php")) . ".xlsx";
if(file_exists($output)) {
    unlink($output);
}

$sheetName = "sheet_1";
$headers = [ "Brand", "Model" ];
$brands = [ "Audi", "Mercedes", "BMW", "Opel" ];
$models = [
    "audi"      =>  [ "A3", "A4", "A5", "A6" ],
    "mercedes"  =>  [ "A140", "B150", "C160", "E200" ],
    "bmw"       =>  [ "M3", "I3", "I4", "I8"],
    "opel"      =>  [ "Astra", "Corsa", "Zafira", "Mokka" ]
];

$export = new Export($output);

$row = 0;
$col = 0;
foreach($headers as $header) {
    $export->addField($sheetName, $row, $col++, $header);
}


foreach($brands as $brand) {
    $key = strtolower($brand);
    foreach($models[$key] as $model) {
        $row++;
        $col = 0;
        $export->addField($sheetName, $row, $col++, $brand);
        $export->addField($sheetName, $row, $col++, $model);
    }
}

$export->setFilter($sheetName, $row = 0, $fromCol = 0, $toCol = 1);
$export->saveOnDisk(__DIR__);
unset($export);

function getValue(int $col) : int | string {
    if($col < 5) {
        return "CELL_" . ($col + 1);
    }

    return (int)($col + 1);
}