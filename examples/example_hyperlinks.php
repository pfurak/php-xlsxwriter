<?php

require_once __DIR__ . '/../vendor/autoload.php'; // Autoload files using Composer autoload

use XLSXWriter\Export;
use XLSXWriter\Hyperlink;
use XLSXWriter\Font;

$output = strtolower(basename(__FILE__, ".php")) . ".xlsx";
if(file_exists($output)) {
    unlink($output);
}

$hyperLinks = [
    new Hyperlink("https://google.com", "Google Tooltip", "Google"),
    new Hyperlink("../images/custom_number_format.png", "Custom Number Format Image Tooltip", "Custom Number Format Image"),
    new Hyperlink(["sheetName" => "sheet_2", "row" => 0, "col" => 0], "Another Sheet Column Tooltip", "Another Sheet Column")
];

$sheetName = "sheet_1";
$maxRow = 3;
$maxCol = 3;

$linkStyle = [
    'font' => new Font([ "underline" => true, "color" => ["from" => "string", "value" => "blue"] ])
];

// Normal mode

$export = new Export($output);
$export->addField("sheet_2", 0, 0, "Column");
for($row = 0; $row < $maxRow; $row++) {
    for($col = 0; $col < $maxCol; $col++) {
        $value = getValue($col, $hyperLinks);
        $style = $value instanceof Hyperlink ? $linkStyle : [];
        $export->addField($sheetName, $row, $col, $value, $style);
    }
}
$export->saveOnDisk(__DIR__);
unset($export);

// With Temp Files

$output = strtolower(basename(__FILE__, ".php")) . "_temp_files.xlsx";
if(file_exists($output)) {
    unlink($output);
}

$useTempFiles = true; // add this parameter if you want to work with huge data
$export = new Export($output, Export::DEFAULT_ENCODING, $useTempFiles);

$export->openRow("sheet_2", 0);
$export->writeCell("sheet_2", 0, "Column");
$export->closeCurrentRow("sheet_2");

for($row = 0; $row < $maxRow; $row++) {
    $export->openRow($sheetName, $row);

    for($col = 0; $col < $maxCol; $col++) {
        $value = getValue($col, $hyperLinks);
        $style = $value instanceof Hyperlink ? $linkStyle : [];

        $export->writeCell($sheetName, $col, $value, $style);
    }

    $export->closeCurrentRow($sheetName);
}

$export->saveOnDisk(__DIR__);
unset($export);

function getValue(int $col, array $hyperLinks) : int | string | Hyperlink {
    $rand = rand(1, 3);
    if($rand % 2 === 1 && count($hyperLinks) > 0) {
        return getHyperLink($hyperLinks);
    }

    if($col < 5) {
        return "CELL_" . ($col + 1);
    }

    return (int)($col + 1);
}

function getHyperLink(array $hyperLinks) : Hyperlink {
    $index = rand(0, count($hyperLinks) - 1);
    return $hyperLinks[$index];
}