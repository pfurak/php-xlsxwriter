<?php

echo "Script started at: ", date('Y-m-d H:i:s'), "\n";

$files = glob('example_*.php');
foreach($files as $file) {
    runCommand($file);
}

echo "Script ended at: ", date('Y-m-d H:i:s'), "\n";

function runCommand($fileName) {
    $command = "php " . $fileName;
    echo "Run current command: ", $command, "\n";
    $output = [];
    exec($command, $output);

    if(count($output) > 0) {
        echo "Output:\n";
        print_r($output);
        echo "\n";
    }
}