<?php

require_once __DIR__ . '/../vendor/autoload.php'; // Autoload files using Composer autoload

use XLSXWriter\Export;

$output = strtolower(basename(__FILE__, ".php")) . ".xlsx";
if(file_exists($output)) {
    unlink($output);
}

$sheetName = "sheet_1";
$maxRow = 3;
$maxCol = 10;
$rowHeight = 50;
$columnWidth = 100;

$export = new Export($output);

for($i = 0; $i < $maxCol; $i++) {
    $export->setColumnWidth($sheetName, $i, $columnWidth - $i * 10);
}

for($row = 0; $row < $maxRow; $row++) {
    $export->setRowHeight($sheetName, $row, $rowHeight - $row * 10);
    for($col = 0; $col < $maxCol; $col++) {
        $value = getValue($col);
        $export->addField($sheetName, $row, $col, $value);
    }
}

$export->saveOnDisk(__DIR__);
unset($export);

$output = strtolower(basename(__FILE__, ".php")) . "_temp_files.xlsx";
if(file_exists($output)) {
    unlink($output);
}

$useTempFiles = true; // add this parameter if you want to work with huge data
$export = new Export($output, Export::DEFAULT_ENCODING, $useTempFiles);

for($i = 0; $i < $maxCol; $i++) {
    $export->setColumnWidth($sheetName, $i, $columnWidth - $i * 10);
}

for($row = 0; $row < $maxRow; $row++) {
    $export->setRowHeight($sheetName, $row, $rowHeight - $row * 10);
    $export->openRow($sheetName, $row);

    for($col = 0; $col < $maxCol; $col++) {
        $value = getValue($row, $col);
        $export->writeCell($sheetName, $col, $value);
    }

    $export->closeCurrentRow($sheetName);
}

$export->saveOnDisk(__DIR__);
unset($export);

function getValue(int $col) : int | string {
    if($col < 5) {
        return "CELL_" . ($col + 1);
    }

    return (int)($col + 1);
}