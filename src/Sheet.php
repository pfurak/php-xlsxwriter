<?php

namespace XLSXWriter;

use XLSXWriter\Chart\Chart;

class Sheet {
    private array $fields;
    private array $rowHeights;
    private array $colWidths;
    private array $mergedCells;
    private array $hyperLinks;
    private ?Drawing $drawing;
    private FileWriter $fileWriter;
    private array $savedRows;
    private int $fixedTopRows;
    private int $fixedLeftColumns;
    private array $filterOptions;

    public function __construct(private int $id, private SharedStrings &$sharedStrings, private Style &$style, private bool $useTempFiles) {
        $this->fields = [];
        $this->rowHeights = [];
        $this->colWidths = [];
        $this->mergedCells = [];
        $this->hyperLinks = [];
        $this->drawing = null;

        if($this->useTempFiles) {
            $this->fileWriter = new FileWriter("sheet_" . $this->id);
            $this->savedRows = [];
        }

        $this->fixedTopRows = -1;
        $this->fixedLeftColumns = -1;
        $this->filterOptions = [];
    }

    public function __destruct() {
        unset($this->fields);
        unset($this->rowHeights);
        unset($this->colWidths);
        unset($this->mergedCells);
        if(!is_null($this->drawing)) {
            unset($this->drawing);
        }

        if($this->useTempFiles) {
            unset($this->fileWriter);
            unset($this->savedRows);
        }

        while(count($this->hyperLinks) > 0) {
            $hyperLink = array_shift($this->hyperLinks);
            unset($hyperLink);
        }
        unset($this->hyperLinks);
    }

    public function getID() : int {
        return $this->id;
    }

    public function getDrawing() : Drawing | null {
        return $this->drawing;
    }

    public function addImage($id, $image, $extension, $row, $col) {
        if(is_null($this->drawing)) {
            $this->drawing = new Drawing();
        }

        $this->drawing->addImage($id, $image, $extension, $row, $col);
    }

    public function addChart(Chart &$chart) {
        if(is_null($this->drawing)) {
            $this->drawing = new Drawing();
        }

        $this->drawing->addChart($chart);
    }

    public function setRowHeight(int $row, int | float $height) {
        $this->rowHeights[$row] = $height;
    }

    public function setColumnWidth(int $column, int | float $width) {
        $this->colWidths[$column] = $width;
    }

    private function initHyperLink(int $row, int $col, Hyperlink $link) : string {
        $id = count($this->hyperLinks) + 1;
        $this->hyperLinks[$id] = clone $link;

        $this->hyperLinks[$id]->setRef(self::getXLSCell($row, $col));
        if(!$this->hyperLinks[$id]->isSheetLink()) {
            $this->hyperLinks[$id]->setID($this->id + $id);
        }
        
        return $this->hyperLinks[$id]->getDisplayText();  
    }

    public function addField(int $row, int $col, Hyperlink | string | int | float $value, $style = array()) {
        if(!isset($this->fields[$row])) {
            $this->fields[$row] = [];
        }

        $prompt = $value instanceof Hyperlink ? $this->initHyperLink($row, $col, $value) : $value;
        $this->fields[$row][$col] = array("value" => $prompt, "style" => $style);
    }

    public function markMergedCells($start, $end) {
        $this->mergedCells[] = [
            "start" =>  $start,
            "end"   =>  $end
        ];
    }

    public function markFixedTopRows(int $rowNumber) {
        $this->fixedTopRows = $rowNumber;
    }

    public function markFixedLeftColumns(int $columnNumber) {
        $this->fixedLeftColumns = $columnNumber;
    }

    public function setFilter(int $row, int $fromCol, int $toCol) {
        $this->filterOptions = [ "row" => $row, "fromCol" => $fromCol, "toCol" => $toCol];
    }

    public function openRow(int $row, string $encoding) {
        $height = isset($this->rowHeights[$row]) && $this->rowHeights[$row] > 0 ? 'ht="' . $this->rowHeights[$row] . '" customHeight="1"' : "";

        if(count($this->savedRows) === 0) {
            $xml = $this->getXMLHeader($encoding);
            $this->fileWriter->writeToFile($xml);

            $this->fileWriter->writeToFile('<row r="' . ($row + 1) . '" ' . $height . ' x14ac:dyDescent="0.25">');
            $this->savedRows[$row] = [ "status" => "open", "lastCell" => -1];
        }
        else {
            $lastRow = array_key_last($this->savedRows);
            if($row <= $lastRow) {
                throw new \InvalidArgumentException("row parameter is lesser or equal than last saved row");
            }

            if($this->savedRows[$lastRow]["status"] === "open") {
                $this->closeCurrentRow();
            }

            $this->fileWriter->writeToFile('<row r="' . ($row + 1) . '" ' . $height . ' x14ac:dyDescent="0.25">');
            $this->savedRows[$row] = [ "status" => "open", "lastCell" => -1];
        }
    }

    public function writeCell($encoding, $cell, $value, $style) {
        if(count($this->savedRows) > 0) {
            $lastRow = array_key_last($this->savedRows);

            if($this->savedRows[$lastRow]["status"] === "closed") {
                throw new \Exception("Last row is closed so cannot write out cell");
            }

            if($cell <= $this->savedRows[$lastRow]["lastCell"]) {
                throw new \Exception("cell parameter is lesser or equal than last saved cell in the opened row");
            }

            $prompt = $value instanceof Hyperlink ? $this->initHyperLink($lastRow, $cell, $value) : $value;
            $tmpCell = $this->finalizeCell([ "value" => $prompt, "style" => $style], $encoding);
            $xml = $this->getCellXML($lastRow, $cell, $tmpCell);
            $this->fileWriter->writeToFile($xml);
            $this->savedRows[$lastRow]["lastCell"] = $cell;
        }
    }

    public function closeCurrentRow() {
        if(count($this->savedRows) > 0) {
            $lastRow = array_key_last($this->savedRows);
            if($this->savedRows[$lastRow]["status"] === "open") {
                $this->fileWriter->writeToFile('</row>');
                $this->savedRows[$lastRow]["status"] ="closed";
            }
        }
    }

    public function finalize($encoding) {
        if(!$this->useTempFiles) {
            if(count($this->mergedCells) > 0) {
                foreach($this->mergedCells as $mergedCell) {
                    $start = $mergedCell['start'];
                    $end = $mergedCell['end'];

                    $startRow = $start['row'];
                    $startCol = $start['col'];
                    $endRow = $end['row'];
                    $endCol = $end['col'];

                    if(!isset($this->fields[$startRow])) {
                        $this->fields[$startRow] = [];
                    }

                    if(!isset($this->fields[$startRow][$startCol])) {
                        $this->fields[$startRow][$startCol] = [ "value" => "", "style" => [] ];
                    }

                    for($row = $startRow; $row <= $endRow; $row++) {
                        for($col = $startCol; $col <= $endCol; $col++) {
                            $this->fields[$row][$col] = $this->fields[$startRow][$startCol];
                        }
                    }
                }
            }

            $this->finalizeFields($encoding);
        }
    }

    private function finalizeFields(string $encoding) {
        ksort($this->fields);
        foreach($this->fields as $row => &$columns) {
            ksort($columns);
            foreach($columns as $col => $cell) {
                $this->fields[$row][$col] = $this->finalizeCell($cell, $encoding);
            }
        }
    }

    private function finalizeCell($cell, $encoding) : array {
        $value = $cell['value'];
        $style = $cell['style'];
        $type = gettype($value);

        if($type === "string") {
            if(!mb_detect_encoding($value, $encoding, true)) {
                $value = @mb_convert_encoding($value, $encoding);
            }
            $value = htmlspecialchars($value, ENT_COMPAT, $encoding);
            $value = $this->sharedStrings->getStringID($value);
        }
        
        $cell['type'] = $type;
        $cell['value'] = $value;
        $cell['style'] = $this->style->getStyleID($style);

        return $cell;
    }

    public static function getXLSCell($row, $col, $absolute = false) : string {
		$n = $col;
		for($r = ""; $n >= 0; $n = intval($n / 26) - 1) {
			$r = chr($n % 26 + 0x41) . $r;
		}

		if ($absolute) {
			return '$' . $r . '$' . ($row + 1);
		}
		return $r . ($row + 1);
	}

    private function getCellXML($row, $col, $column) : string {
        $cell = self::getXLSCell($row, $col);
        $style = $column['style'] > 0 ? ' s="' . $column['style'] . '"' : '';
        $xml = '';
        if($column['type'] === "string") {
            $xml .= '<c r="' . $cell . '" t="s"' . $style . '>' . PHP_EOL;
                $xml .= '<v>' . $column['value'] . '</v>' . PHP_EOL;
            $xml .= '</c>' . PHP_EOL;
        }
        else if($column['type'] === "double") {
            $xml .= '<c r="' . $cell . '"' . $style . '>' . PHP_EOL;
                $xml .= '<v>' . (double)$column['value'] . '</v>' . PHP_EOL;
            $xml .= '</c>' . PHP_EOL;
        }
        else if($column['type'] === "float") {
            $xml .= '<c r="' . $cell . '"' . $style . '>' . PHP_EOL;
                $xml .= '<v>' . (float)$column['value'] . '</v>' . PHP_EOL;
            $xml .= '</c>' . PHP_EOL;
        }
        else { // integer
            $xml .= '<c r="' . $cell . '"' . $style . '>' . PHP_EOL;
                $xml .= '<v>' . (int)$column['value'] . '</v>' . PHP_EOL;
            $xml .= '</c>' . PHP_EOL;
        }
        return $xml;
    }

    private function getMergedCellXML($mergedCell) : string {
        $startCell = self::getXLSCell($mergedCell['start']['row'], $mergedCell['start']['col']);
        $endCell = self::getXLSCell($mergedCell['end']['row'], $mergedCell['end']['col']);
        return '<mergeCell ref="' . $startCell . ':' . $endCell . '"/>' . PHP_EOL;
    }

    private function getSheetViews() : string {
        $xml = '<sheetViews>';
            $xml .= '<sheetView workbookViewId="0"' . ($this->id === 1 ? ' tabSelected="1"' : '') . '>';
                if($this->fixedTopRows > -1 && $this->fixedLeftColumns > -1) {
                    $xml .= '<pane xSplit="' . ($this->fixedTopRows + 1) . '" ySplit="' . ($this->fixedLeftColumns + 1) . '" topLeftCell="' . self::getXLSCell($this->fixedTopRows + 1, $this->fixedLeftColumns + 1) . '" activePane="bottomRight" state="frozen"/>';
                    $xml .= '<selection pane="topRight"/>';
                    $xml .= '<selection pane="bottomLeft"/>';
                    $xml .= '<selection pane="bottomRight" activeCell="A1" sqref="A1"/>';
                }
                else if($this->fixedTopRows > -1) {
                    $xml .= '<pane ySplit="' . ($this->fixedTopRows + 1) . '" topLeftCell="' . self::getXLSCell($this->fixedTopRows + 1, 0) . '" activePane="bottomLeft" state="frozen"/>';
                    $xml .= '<selection pane="bottomLeft" activeCell="A1" sqref="A1" />';
                }
                else if($this->fixedLeftColumns > -1) {
                    $xml .= '<pane xSplit="' . ($this->fixedLeftColumns + 1) . '" topLeftCell="' . self::getXLSCell(0, $this->fixedLeftColumns + 1) . '" activePane="topRight" state="frozen"/>';
                    $xml .= '<selection pane="topRight" activeCell="A1" sqref="A1" />';
                }
            $xml .= '</sheetView>';
        $xml .= '</sheetViews>';

        return $xml;
    }

    private function getColWidths() : string {
        $xml = "";
        if(count($this->colWidths) > 0) {
            $xml .= "<cols>";
            foreach($this->colWidths as $key => $value) {
                $xml .= '<col min="' . ($key + 1) . '" max="' . ($key + 1) . '" width="' . ((float)$value + 0.83) . '" customWidth="1"/>';
            }
            $xml .= "</cols>";
        }
        return $xml;
    }

    private function getXMLHeader($encoding) : string {
        $xml = '<?xml version="1.0" encoding="' . $encoding . '" standalone="yes"?>';
        $xml .= '<worksheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" mc:Ignorable="x14ac" xmlns:x14ac="http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac">';
            $xml .= $this->getSheetViews();
            $xml .= '<sheetFormatPr baseColWidth="10" defaultRowHeight="15" x14ac:dyDescent="0.25"/>';
            $xml .= $this->getColWidths();
            $xml .= '<sheetData>';
        return $xml;
    }

    private function getFilter() : string {
        if(count($this->filterOptions) === 0) {
            return '';
        }

        $fromCell = self::getXLSCell($this->filterOptions['row'], $this->filterOptions['fromCol']);
        $toCell = self::getXLSCell($this->filterOptions['row'], $this->filterOptions['toCol']);
        return '<autoFilter ref="' . $fromCell . ':' . $toCell . '"/>';
    }

    private function getXMLFooter() : string {
        $mergedCellsCounter = count($this->mergedCells);

            $xml = '</sheetData>';
            if(count($this->hyperLinks) > 0) {
                $xml .= '<hyperlinks>';
                foreach($this->hyperLinks as $hyperLink) {
                    $xml .= $hyperLink->getXML();
                }
                $xml .= '</hyperlinks>';
            }
            if($mergedCellsCounter > 0) {
                $xml .= '<mergeCells count="' . $mergedCellsCounter . '">';
                    foreach($this->mergedCells as $mergedCell) {
                        $xml .= $this->getMergedCellXML($mergedCell);
                    }
                $xml .= '</mergeCells>';
            }

            $xml .= $this->getFilter();
            $xml .= '<pageMargins left="0.7" right="0.7" top="0.75" bottom="0.75" header="0.3" footer="0.3"/>';

            if(!is_null($this->drawing) && $this->drawing instanceof Drawing) {
                $xml .= '<drawing r:id="rId' . $this->id . '" />';
            }
        $xml .= '</worksheet>';
        return $xml;
    }

    public function getTempFile() : string {
        $xml = $this->getXMLFooter();
        $this->fileWriter->writeToFile($xml);

        return $this->fileWriter->getTempFile();
    }

    public function getXML($encoding) : string {
        $xml = $this->getXMLHeader($encoding);

        foreach($this->fields as $row => $columns) {
            $height = isset($this->rowHeights[$row]) && $this->rowHeights[$row] > 0 ? 'ht="' . $this->rowHeights[$row] . '" customHeight="1"' : "";

            $xml .= '<row r="' . ($row + 1) . '" ' . $height . ' x14ac:dyDescent="0.25">';
                foreach($columns as $columnIndex => $column) {
                    $xml .= $this->getCellXML($row, $columnIndex, $column);
                }
            $xml .= '</row>';
        }

        $xml .= $this->getXMLFooter();

        return $xml;
    }

    public function getRelationXML($encoding) : string {
        $xml = '<?xml version="1.0" encoding="' . $encoding . '" standalone="yes"?>' . PHP_EOL;
        $xml .= '<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">' . PHP_EOL;
            if(!is_null($this->drawing) && $this->drawing instanceof Drawing) {
                $xml .= '<Relationship Id="rId' . $this->id . '" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/drawing" Target="../drawings/drawing' . $this->id . '.xml"/>' . PHP_EOL;
            }
            
            foreach($this->hyperLinks as $hyperLink) {
                $xml .= $hyperLink->getRelationXML();
            }
        $xml .= '</Relationships>' . PHP_EOL;

        return $xml;
    }
}