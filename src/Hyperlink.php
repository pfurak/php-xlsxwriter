<?php

namespace XLSXWriter;

class Hyperlink {
    private int $id = -1;
    private string $ref;
    private bool $sheetLink;

    public function __construct(private string | array $target, private string $tooltip = "", private string $display = "") {
        if(is_array($this->target)) {
            $this->sheetLink = true;
            if(!$this->checkIfValidTarget()) {
                throw new \InvalidArgumentException("Invalid target parameter");
            }

            $this->target = $this->target['sheetName'] . '!' . Sheet::getXLSCell($this->target['row'], $this->target['col']);
        }
        else {
            $this->sheetLink = false;
        }

        if(strlen($this->display) === 0) {
            $this->display = $target;
        }
    }

    private function checkIfValidTarget() : bool {
        if(is_array($this->target)) {
            $keys = ["sheetName", "row", "col"];
            foreach($keys as $value) {
                if(!isset($this->target[$value]) || strlen($this->target[$value]) === 0) {
                    return false;
                }
            }
        }

        return true;
    }

    public function isSheetLink() : bool {
        return $this->sheetLink;
    }

    public function getDisplayText() : string {
        return $this->display;
    }

    public function setRef(string $ref) {
        $this->ref = $ref;
    }

    public function setID(int $id) {
        if(!$this->sheetLink && $this->id === -1) {
            if($id < 1) {
                throw new \InvalidArgumentException("ID cannot be less than 1");
            }
    
            $this->id = $id;
        }
    }

    public function getXML() : string {
        $xml = '<hyperlink ref="' . $this->ref . '"';
        if(strlen($this->tooltip) > 0) {
            $xml .= ' tooltip="' . $this->tooltip . '"';
        }

        if($this->sheetLink) {
            if(strlen($this->display) > 0) {
                $xml .= ' display="' . $this->display . '"';
            }
            $xml .= ' location="' . $this->target . '"';
        }
        else {
            $xml .= ' r:id="rId' . $this->id . '"';
        }
        $xml .= ' />';
        return $xml;
    }

    public function getRelationXML() : string {
        if(!$this->sheetLink) {
            return '<Relationship Id="rId' . $this->id . '" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink" Target="' . $this->target . '" TargetMode="External"/>';
        }
        return "";
    }
}

